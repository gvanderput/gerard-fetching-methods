Check out the different branches ("axios", "fetchApi", etc.) in this repository to see all examples.

To list all branches, run:

$ git branch -a

To check out a specific branch, f.e. the "axios" branch, run:

$ git checkout axios
