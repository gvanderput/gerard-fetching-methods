import React from "react";
import ReactDOM from "react-dom/client";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <p>Check out the different branches ("axios", "fetchApi", etc.) in this repository to see all examples.</p>
    <p>To list all branches, run:</p>
    <code>$ git branch -a</code>
    <p>To check out a specific branch, f.e. the "axios" branch, run:</p>
    <code>$ git checkout axios</code>
  </React.StrictMode>
);
